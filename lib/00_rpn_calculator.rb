class RPNCalculator

  def initialize
    @array = []
    @value = []
  end

  def push(num)
    @array << num
  end

  def plus
    raise error if @array == []
    @value = @array.pop(2).reduce(:+)
    @array << @value
  end

  def minus
    raise error if @array == []
    @value = @array.pop(2).reduce(:-)
    @array << @value
  end

   def times
    raise error if @array == []
    @value = @array.pop(2).reduce(:*)
    @array << @value
  end

   def divide
    raise error if @array == []
    @value = @array.pop(2).map{|num| num.to_f}.reduce(:/)
    @array << @value
    end

    def value
      return @value
      @value = []
    end

    def error
      "calculator is empty"
    end

    def tokens(string)
      characters = string.split
      characters.map do |ch|
        "123456789".include?(ch) ? ch.to_i : ch.to_sym
      end
    end

    def evaluate(string)
      token = tokens(string)
      token.each do |el|
        if (1..9).include?(el)
          @array << el
        elsif el == :+
          self.plus
        elsif el == :-
          self.minus
        elsif el == :*
          self.times
        elsif el == :/
          self.divide
        end
      end
      self.value
    end

end
